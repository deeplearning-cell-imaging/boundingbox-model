from datetime import datetime
import os
import time
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import keras
import keras_retinanet
from keras_retinanet.preprocessing.csv_generator import CSVGenerator
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color
from keras_retinanet.utils.eval import evaluate
import cv2
from keras_retinanet.bin.train import create_models, create_callbacks

from keras_retinanet.utils.anchors import make_shapes_callback, AnchorParameters

from archive import archive_training_history
from visualisations import plot_results


# NUM_EPOCHS = 3

np.random.seed(5)

def split_data(parent_dir, shortened, validation_split = 0.3):
    data_file = os.path.join(parent_dir, "training_data{}.csv".format("" if not shortened else "_short"))

    main_df = pd.read_csv(data_file, header = None, names = ['file', 'x1', 'x2', 'y1', 'y2', 'class'], 
    keep_default_na=True)

    main_df = main_df.astype(dtype = {'x1': pd.Int64Dtype(), 'x2': pd.Int64Dtype(), 
    'y1': pd.Int64Dtype(), 'y2': pd.Int64Dtype()})

    filenames = main_df['file'].unique()
    np.random.shuffle(filenames)
    split_location = int(len(filenames) * (1 - validation_split))
    training_names = filenames[:split_location]
    validation_names = filenames[split_location:]

    validation_df = main_df[main_df['file'].isin(validation_names)]
    validation_df.to_csv(os.path.join(parent_dir, "validation_subset{}.csv".format("_short" if shortened else "")), header = False, index = False)


    training_df = main_df[main_df['file'].isin(training_names)]
    training_df.to_csv(os.path.join(parent_dir, "training_subset{}.csv".format("_short" if shortened else "")), header = False, index = False)

    print("Making sure names are different: {}".format(sum(validation_df['file'].isin(training_df['file']))))
    print(validation_df['file'].unique())
    print(training_df['file'].unique())

def short_subset(parent_dir, subset_size = 10):
    
    data_file = os.path.join(parent_dir, "training_data.csv")

    main_df = pd.read_csv(data_file, header = None, names = ['file', 'x1', 'x2', 'y1', 'y2', 'class'], 
    keep_default_na=True)

    small_subset = main_df.sample(n = subset_size)

    small_subset.to_csv(os.path.join(parent_dir, "training_data_short.csv"), header = False, index = False)

    
def train_model(n_epochs, include_validation, smaller_subset, run_training, 
    backbone = "resnet50", experiment = "base_training", args = None, parent_dir = "images"):

    todays_date = datetime.today().date()

    # weights_dir = 'pretrained'

    # pretrained_filepaths = {

    # }

    num_classes = pd.read_csv(os.path.join(parent_dir, "class_IDs.csv"), header=None).shape[0]


    exp_filename = "_{}_{}_{}".format(backbone, experiment, parent_dir if parent_dir == "images" else "malaria")

    print(exp_filename)

    mybackbone = models.backbone(backbone)

    myweights = mybackbone.download_imagenet()

    print("Num Classes:{}".format(num_classes))

    print("Backbone Backbone:") 
    # print(mybackbone.backbone)

    mymodel = models.backbone(backbone).retinanet(num_classes=num_classes)

    mymodel.load_weights(myweights, by_name = True, skip_mismatch = True)
    common_args = {
        'preprocess_image': models.backbone(backbone).preprocess_image,
    }

    mymodel.compile(
        loss={
            'regression': keras_retinanet.losses.smooth_l1(),
            'classification': keras_retinanet.losses.focal()
        },
        optimizer=keras.optimizers.adam(lr=1e-5, clipnorm=0.001)
    )

    if smaller_subset:
        short_subset(parent_dir)
        if include_validation:
            ds_filename = os.path.join(parent_dir, "training_subset_short.csv")
            ds_filename_val = os.path.join(parent_dir, "validation_subset_short.csv")
        else:
            ds_filename = os.path.join(parent_dir, "training_data_short.csv")
    else:
        if include_validation:
            ds_filename = os.path.join(parent_dir, "training_subset.csv")
            ds_filename_val = os.path.join(parent_dir, "validation_subset.csv")
        else:
            ds_filename = os.path.join(parent_dir, "training_data.csv")


    # split into training and testing 
    if include_validation:
        split_data(parent_dir, smaller_subset)
        mygenerator = CSVGenerator(ds_filename, os.path.join(parent_dir, "class_IDs.csv"), batch_size = 32, **common_args)
        my_val_generator = CSVGenerator(ds_filename_val, os.path.join(parent_dir, "class_IDs.csv"), batch_size = 32,  **common_args)
    else:
        mygenerator = CSVGenerator(ds_filename, os.path.join(parent_dir, "class_IDs.csv"), batch_size = 32, **common_args)

    # model, training_model, prediction_model = create_models(
    #     backbone_retinanet=mybackbone.retinanet,
    #     num_classes= 1,
    #     weights=mybackbone.download_imagenet(),
    #     # multi_gpu=args.multi_gpu,
    #     # freeze_backbone=args.freeze_backbone,
    #     # lr=args.lr,
    #     lr = 1e-6,
    #     # config=args.config
    # )

    # callbacks = create_callbacks(
    #     model,
    #     training_model,
    #     prediction_model,
    #     my_val_generator,
    #     args
    # )

    checkpoint = keras.callbacks.ModelCheckpoint('checkpoint_weights/weights{}_{}.h5'.format(exp_filename, todays_date), 
    monitor='loss', save_weights_only=True, save_best_only=True)
    if run_training:
        start = time.time()
        history = mymodel.fit_generator(mygenerator, 
            epochs=n_epochs, 
            shuffle=True, 
            callbacks=[checkpoint], 
            validation_data = None if not include_validation else my_val_generator, 
            workers = 4)
        print("processing time: ", time.time() - start)

        archive_training_history(history.history, 'initial_history{}_{}'.format(exp_filename, todays_date))

        plot_results('results/initial_history{}_{}.txt'.format(exp_filename, todays_date))
    else:
        # mymodel = models.backbone(backbone).retinanet(num_classes=1)
        mybackbone = models.backbone(backbone)

        myweights = mybackbone.download_imagenet()

        print("Backbone Backbone:") 
        # print(mybackbone.backbone)

        mymodel = models.backbone(backbone).retinanet(num_classes=num_classes)

        mymodel.load_weights(myweights, by_name = True, skip_mismatch = True)

        


        # mymodel.summary()

        # mybackbone

        # mymodel.load_weights('vacc_trained/weights_vgg16_backbone_testing_2020-04-29.h5')

    my_val_generator.compute_shapes = make_shapes_callback(mymodel)
    labels_to_names = {0: 'channel 1'}

    # # load image
    # image = read_image_bgr('images/channel_0_img9.tif')

    # draw = image.copy()
    # draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

    # # # preprocess image for network
    # # image = preprocess_image(image)
    # # image, scale = resize_image(image)

    # # # process image
    # # start = time.time()
    # boxes, scores = mymodel.predict_on_batch(np.expand_dims(image, axis=0))

    # # print("processing time: ", time.time() - start)

    # # correct for image scale
    # # boxes /= scale

    # # visualize detections
    # for box, score in zip(boxes[0], scores[0]):
    #     # scores are sorted so we can break
    #     #     if score < 0.3:
    #     #         break
    #     color = label_color(3)

    #     b = box.astype(int)
    #     draw_box(draw, b, color=[225, 0, 0])

    #     caption = "{} {:.3f}".format(labels_to_names[0], score[0])
    #     draw_caption(draw, b, caption)

    # plt.figure(figsize=(15, 15))
    # plt.axis('off')
    # plt.imshow(draw)
    # plt.show()

    # print(evaluate(my_val_generator, mymodel))

    #Default
    # myanchors = AnchorParameters(
    #     sizes   = [8, 16, 32, 64, 128],
    #     strides = [8, 16, 32, 64, 128],
    #     ratios  = np.array([0.5, 1, 2], keras.backend.floatx()),
    #     scales  = np.array([2 ** 0, 2 ** (1.0 / 3.0), 2 ** (2.0 / 3.0)], keras.backend.floatx()),
    # )

    # De Optimized
    # myanchors = AnchorParameters(
    #     sizes   = [8, 16, 32, 64, 128],
    #     strides = [8, 16, 32, 64, 128],
    #     ratios  = [0.638, 1.0, 1.568],
    #     scales  = [0.4, 0.5, 0.634]
    # )

    # converted_model = models.convert_model(mymodel, anchor_params = myanchors, nms = False,)

    # # average_precisions, inference_time = evaluate(
    # eval_results = evaluate(
    #         my_val_generator,
    #         converted_model,
    #         # iou_threshold=args.iou_threshold,
    #         score_threshold= 0,
    #         iou_threshold = 0.9,
    #         max_detections=1000,
    #         save_path="results2/"
    #     )

    # print(eval_results)

    # # print evaluation
    # total_instances = []
    # precisions = []
    # for label, (average_precision, num_annotations) in eval_results.items():
    #     print('{:.0f} instances of class'.format(num_annotations),
    #             my_val_generator.label_to_name(label), 'with average precision: {:.4f}'.format(average_precision))
    #     total_instances.append(num_annotations)
    #     precisions.append(average_precision)

    # if sum(total_instances) == 0:
    #     print('No test instances found.')
    #     return

    # # print('Inference time for {:.0f} images: {:.4f}'.format(my_val_generator.size(), inference_time))

    # print('mAP using the weighted average of precisions among classes: {:.4f}'.format(sum([a * b for a, b in zip(total_instances, precisions)]) / sum(total_instances)))
    # print('mAP: {:.4f}'.format(sum(precisions) / sum(x > 0 for x in total_instances)))

    # return sum(precisions) / sum(x > 0 for x in total_instances)

    return 0

if __name__ == "__main__":
    # split_data()
    split_data(True)
    exit()

    # train_model(n_epochs = 3, smaller_subset = True, include_validation = True)