"""Scipt for predicting bounding boxes from different backbones. 
Takes the weights that have been trained, converts the model to
 an inference model then uses that inference model to draw bounding 
 boxes with anchors defined.  

This script if works for generating images, the mAP is not working, 
for generating mAP please see the README.md
"""
import time
import numpy as np
import matplotlib.pyplot as plt

import keras
import keras_retinanet
from keras_retinanet.preprocessing.csv_generator import CSVGenerator
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption, draw_boxes
from keras_retinanet.utils.eval import evaluate
from keras_retinanet.utils.colors import label_color
import cv2

from keras_retinanet.utils.anchors import make_shapes_callback, AnchorParameters

from archive import archive_training_history
from visualisations import plot_results

def main():
    backbones = [
        'resnet50',
        'mobilenet160_1',
        'vgg16',
         'resnet9'
         ]

    for backbone in backbones:
        predict_w_backpone(backbone)
    

def predict_w_backpone(backbone):
    """Function for drawing the bounding boxes using the inference model 
    from ReitinNet. From the sepcified backbone, load the correct rates, 
    and predcit images and save to the corresponding directory. 

    Arguments:
        backbone {str} -- String of allowed backbones.
    """
    
    backbone_standard = backbone if backbone != 'resnet9' else 'resnet50'

    mymodel = models.backbone(backbone_standard).retinanet(num_classes=1)

    # mymodel.summary()
    
    weight_files = {
        "resnet50" : "vacc_trained/weights/weights_resnet50_backbone_testing_images_2020-05-03.h5",
        'resnet9' : 'vacc_trained/weights/resnet50_csv_09.h5', 
        'vgg16' : 'vacc_trained/weights/weights_vgg16_backbone_testing_images_2020-05-04.h5',
        'mobilenet160_1' : 'vacc_trained/weights/weights_mobilenet160_1_backbone_testing_images_2020-05-03.h5'
    }

    mymodel.load_weights(weight_files[backbone])

    # Anchors found by DE 
    myanchors = AnchorParameters(
        sizes   = [32, 64, 128, 256, 512],
        strides = [8, 16, 32, 64, 128],
        ratios  = [0.629, 1.0, 1.589],
        scales  = [0.4, 0.501, 0.638]
    )

    mymodel.compile(
        loss={
            'regression': keras_retinanet.losses.smooth_l1(),
            'classification': keras_retinanet.losses.focal()
        },
        optimizer=keras.optimizers.adam(lr=1e-5, clipnorm=0.001)
    )

    converted_model = models.convert_model(mymodel, anchor_params = myanchors, nms = True)

    mygenerator = CSVGenerator("images/validation_subset.csv", "images/class_IDs.csv")

    if 'vgg' in backbone:
        mygenerator.compute_shapes = make_shapes_callback(mymodel)


    eval_results = evaluate(mygenerator, converted_model, save_path='results_{}/'.format(backbone), 
                score_threshold=0.5,
                iou_threshold = 0.5,
                max_detections=100)

    print(eval_results)
    total_instances = []
    precisions = []
    for label, (average_precision, num_annotations) in eval_results.items():
        print('{:.0f} instances of class'.format(num_annotations),
                mygenerator.label_to_name(label), 'with average precision: {:.4f}'.format(average_precision))
        total_instances.append(num_annotations)
        precisions.append(average_precision)

    if sum(total_instances) == 0:
        print('No test instances found.')

    print('mAP using the weighted average of precisions among classes: {:.4f}'.format(sum([a * b for a, b in zip(total_instances, precisions)]) / sum(total_instances)))
    print('mAP: {:.4f}'.format(sum(precisions) / sum(x > 0 for x in total_instances)))

if __name__ == "__main__":
    main()
