"""Initial training script, but this script is deprepecated so it's no 
longer used. It's been replaced by train 
"""
from datetime import datetime
import time
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import keras
import keras_retinanet
from keras_retinanet.preprocessing.csv_generator import CSVGenerator
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color
from keras_retinanet.utils.eval import evaluate
import cv2
from keras_retinanet.bin.train import create_models

from archive import archive_training_history
from visualisations import plot_results


NUM_EPOCHS = 3

np.random.seed(5)

def split_data(shortened, validation_split = 0.3):
    data_file = "images/training_data{}.csv".format("" if not shortened else "_short")

    main_df = pd.read_csv(data_file, header = None, names = ['file', 'x1', 'x2', 'y1', 'y2', 'class'], 
    keep_default_na=True)

    main_df = main_df.astype(dtype = {'x1': pd.Int64Dtype(), 'x2': pd.Int64Dtype(), 
    'y1': pd.Int64Dtype(), 'y2': pd.Int64Dtype()})

    filenames = main_df['file'].unique()
    np.random.shuffle(filenames)
    split_location = int(len(filenames) * (1 - validation_split))
    training_names = filenames[:split_location]
    validation_names = filenames[split_location:]

    validation_df = main_df[main_df['file'].isin(validation_names)]
    validation_df.to_csv("images/validation_subset{}.csv".format("_short" if shortened else ""), header = False, index = False)


    training_df = main_df[main_df['file'].isin(training_names)]
    training_df.to_csv("images/training_subset{}.csv".format("_short" if shortened else ""), header = False, index = False)


def short_subset(subset_size = 20):
    
    data_file = "images/training_data.csv"

    main_df = pd.read_csv(data_file, header = None, names = ['file', 'x1', 'x2', 'y1', 'y2', 'class'], 
    keep_default_na=True)

    small_subset = main_df.sample(n = subset_size)

    small_subset.to_csv("images/training_data_short.csv", header = False, index = False)

    
def train_model(backbone = "resnet50", experiment = "base_training", n_epochs = 10, include_validation = True, 
    smaller_subset = False):

    todays_date = datetime.today().date()

    # weights_dir = 'pretrained'

    # pretrained_filepaths = {
    #     'resnet50': "resnet50_coco_best_v2.1.0.h5",

    # }

    exp_filename = "_{}_{}".format(backbone, experiment)


    mybackbone = models.backbone(backbone)

    print("Backbone Backbone:") 
    print(mybackbone.backbone)

    mymodel = models.backbone(backbone).retinanet(num_classes=1)

    mymodel.compile(
        loss={
            'regression': keras_retinanet.losses.smooth_l1(),
            'classification': keras_retinanet.losses.focal()
        },
        optimizer=keras.optimizers.adam(lr=1e-5, clipnorm=0.001)
    )

    # mymodel.load_weights('pretrained/resnet50_coco_best_v2.1.0.h5', by_name=True, skip_mismatch=True)
    # by_name=True to load weights into different architecture with some layers the same

    # weights = mybackbone.download_imagenet()

    if smaller_subset:
        short_subset()
        if include_validation:
            ds_filename = "images/training_subset_short.csv"
            ds_filename_val = "images/validation_subset_short.csv"
        else:
            ds_filename = "images/training_data_short.csv"
    else:
        if include_validation:
            ds_filename = "images/training_subset.csv"
            ds_filename_val = "images/validation_subset.csv"
        else:
            ds_filename = "images/training_data.csv"


    if include_validation:
        split_data(smaller_subset)
        mygenerator = CSVGenerator(ds_filename, "images/class_IDs.csv", base_dir='')
        my_val_generator = CSVGenerator(ds_filename_val, "images/class_IDs.csv", base_dir='')
    else:
        mygenerator = CSVGenerator(ds_filename, "images/class_IDs.csv", base_dir='')

    # mymodel, mytraining_model, myprediction_model = create_models(
    #         backbone_retinanet=mybackbone.retinanet,
    #         num_classes=mygenerator.num_classes(),
    #         weights=weights,
    #         # multi_gpu=args.multi_gpu,
    #         # freeze_backbone=args.freeze_backbone,
    #         # lr=args.lr,
    #         # config=args.config
    #     )

    checkpoint = keras.callbacks.ModelCheckpoint('checkpoint_weights/weights{}_{}.h5'.format(exp_filename, todays_date), 
    monitor='loss', save_weights_only=True, save_best_only=True)

    start = time.time()
    history = mymodel.fit_generator(mygenerator, 
        epochs=NUM_EPOCHS, 
        shuffle=True, 
        callbacks=[checkpoint], 
        validation_data = None if not include_validation else my_val_generator, 
        workers = 6)
    print("processing time: ", time.time() - start)

    archive_training_history(history.history, 'initial_history{}_{}'.format(exp_filename, todays_date))

    plot_results('results/initial_history{}_{}.txt'.format(exp_filename, todays_date))

    labels_to_names = {0: 'channel 1'}

    # # load image
    # image = read_image_bgr('images/channel_0_img9.tif')

    # draw = image.copy()
    # draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

    # # # preprocess image for network
    # # image = preprocess_image(image)
    # # image, scale = resize_image(image)

    # # # process image
    # # start = time.time()
    # boxes, scores = mymodel.predict_on_batch(np.expand_dims(image, axis=0))

    # # print("processing time: ", time.time() - start)

    # # correct for image scale
    # # boxes /= scale

    # # visualize detections
    # for box, score in zip(boxes[0], scores[0]):
    #     # scores are sorted so we can break
    #     #     if score < 0.3:
    #     #         break
    #     color = label_color(3)

    #     b = box.astype(int)
    #     draw_box(draw, b, color=[225, 0, 0])

    #     caption = "{} {:.3f}".format(labels_to_names[0], score[0])
    #     draw_caption(draw, b, caption)

    # plt.figure(figsize=(15, 15))
    # plt.axis('off')
    # plt.imshow(draw)
    # plt.show()

    # print(evaluate(my_val_generator, mymodel))

if __name__ == "__main__":
    # split_data()

    train_model(n_epochs = 3, smaller_subset = True, include_validation = True)