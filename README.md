# CS395 BoundingBox Model

Creating a bounding model for counting cells in flourescent microscopy images. Uses [Fizyr's implementation of RetinaNet using Keras](https://github.com/fizyr/keras-retinanet) to create bounding boxes around DAPI stained nuclei images. 

## Dataset

Images are provided by the Vermont Microscopy Center, containing flouscently labeled tonsil and breat cancer biopsy tissue. 

![Full Tonsil Image](online_imgs/colored_main_cropped.png)


## Prediction
To convert our training models to inference models for evaluation, we ran the evaluate.py script from within the keras-retinanet repository as follows:

`python keras_retinanet/bin/evaluate.py csv /path/to/csv/file/containing/annotations /path/to/csv/file/containing/classes /path/to/saved/training/model`

## Output 
The model returns bounding boxes around the channels of interest, with the percent confidence. 

![Image Output](online_imgs/10.png)

## Authors
* **Caitlin Grasso**
* **Connor Klopfer**
* **Jack Olszewski**

## Built With

* [ketas-retinanet](https://github.com/fizyr/keras-retinanet)
* [keras](https://www.tensorflow.org/guide/keras)




