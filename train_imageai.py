"""Scipt for Utilizing the ImageAI implementation of RetinaNet. 
"""
from imageai.Detection.Custom import DetectionModelTrainer

from imageai.Detection.Custom.gen_anchors import generateAnchors
from imageai.Detection.Custom.voc import parse_voc_annotation

import os
import argparse
import pickle
import xml.etree.ElementTree as ET

parser = argparse.ArgumentParser()
parser.add_argument("-e", default= 10, type = int, help = "Num Epochs")
parser.add_argument('-b', default = 32, type = int, \
help = 'Batch Size')


args = args = vars(parser.parse_args())

trainer = DetectionModelTrainer()
trainer.setModelTypeAsYOLOv3()
trainer.setDataDirectory(data_directory="pascal_format")

trainer.setTrainConfig(object_names_array=["class 1"], 
batch_size=args['b'], num_experiments=args['e'], 
train_from_pretrained_model="pretrained/pretrained-yolov3.h5"
)

trainer.trainModel()