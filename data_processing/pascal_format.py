"""Convert the VMC to a PASCAL-VAC Dataset, to use for the ImageAI 
implementation of retinanet. If you call this script alone, it will 
whrite the annotations in xml format and the put the corresponding 
images in the coreect folder. 
"""
import os
import pandas as pd
import shutil
import numpy as np
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring, ElementTree
from xml.dom import minidom
from PIL import Image



def set_to_pascal(old_dir, validation_split = 0.3):
    """The main function for the conversion, splits the 
    directory into the training and validation set. 

    Arguments:
        old_dir {str} -- the directory of the original images. 

    Keyword Arguments:
        validation_split {float} -- [description] (default: {0.3})
    """

    np.random.seed(5)
    file_path = os.path.join(old_dir, "training_data.csv")

    input_df = pd.read_csv(file_path, names = ['file', 'x1', 'y1', 'x2', 'y2', 'class'])
    input_df = input_df.dropna(axis = 0)
    unique_files = input_df['file'].unique()
    np.random.shuffle(unique_files)
    split_location = int(len(unique_files) * (1 - validation_split))
    training_names = unique_files[:split_location]
    validation_names = unique_files[split_location:]

    build_annotations(old_dir, "train", training_names, input_df)

    build_annotations(old_dir, "validation", validation_names, input_df)

    print(input_df)

def build_annotations(old_dir, data_type, filenames, input_df):   
    """Build the annotations in XML format according to 
    PASCAL VOC format, this needs to be run for training and testing sets

    Arguments:
        old_dir {[type]} -- [description]
        data_type {[type]} -- [description]
        filenames {[type]} -- [description]
        input_df {[type]} -- [description]
    """
    for image in filenames:
        boxes = input_df.loc[input_df['file'] == image]
        top = Element('annotation')
        # folder = SubElement(top, 'folder')
        # folder.text = 'channel_1'
        filename = SubElement(top, 'filename')
        filename.text = image
        path = SubElement(top, 'path')
        path.text = os.path.join(data_type, 'images', image)

        image_opened = np.array(Image.open(os.path.join(old_dir, image)))

        img_width = SubElement(top, 'width')
        img_width.text = str(image_opened.shape[0])
        img_height = SubElement(top, 'height')
        img_height.text = str(image_opened.shape[1])

        image_depth = SubElement(top, 'depth')
        image_depth.text = str(3)


        for idx, box in boxes.iterrows():
            bbox = SubElement(top, 'object')
            name = SubElement(bbox, 'name')
            name.text = box['class']
            pose = SubElement(bbox, 'pose')
            pose.text = "Unspecified"
            truncated = SubElement(bbox, 'truncated')
            truncated.text = 0
            difficult = SubElement(bbox, 'truncated')
            difficult.text = 0
            bndbox = SubElement(bbox, "bndbox")
            xmin = SubElement(bndbox, 'xmin')
            # if box['x1'] == "":
            #     print("NAN found")
            #     break
            xmin.text = str(int(box['x1']))
            ymin = SubElement(bndbox, 'ymin')
            ymin.text = str(int(box['y1']))
            xmax = SubElement(bndbox, 'xmax')
            xmax.text = str(int(box['x2']))
            xmax = SubElement(bndbox, 'ymax')
            xmax.text = str(int(box['y2']))
        
        shutil.copyfile(os.path.join(old_dir, image), 
            os.path.join("pascal_format", data_type, 'images', image))
        rough_string = ET.tostring(top)
        tree = ElementTree(top)
        tree.write(os.path.join("pascal_format", data_type, 'annotations', image.replace(".tif", "")) + '.xml')


if __name__ == "__main__":
    
    set_to_pascal("images")