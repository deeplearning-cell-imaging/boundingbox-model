"""Combine ImageJ generated datasets into on CSV-firendly version
"""
import os
import pandas as pd
import json


def crawl_through_json_files(json_file):
    fin = open(json_file)
    records = []  
    # final[['filename', 'x1', 'y1', 'x2', 'y2', 'class_name']]  
    bb_records = {
        'filename' : [], 
        'x1' : [], 
        'y1' : [], 
        'x2' : [], 
        'y2' : [],
        'class_name' : []
    }
    for file in fin.readlines():
        # records.append(json.loads(file.rstrip()))        
        records = json.loads(file.rstrip())
        for record in records:
            for box in record['objects']:
                bb_records['filename'].append(record['image']['pathname'].split('/')[-1])
                bb_records['y1'].append(box['bounding_box']['minimum']['r'])
                bb_records['x1'].append(box['bounding_box']['minimum']['c'])
                bb_records['y2'].append(box['bounding_box']['maximum']['r'])
                bb_records['x2'].append(box['bounding_box']['maximum']['c'])
                bb_records['class_name'].append(box['category'])
            # exit()        
        # filename = record
    final_df = pd.DataFrame(bb_records)
    print(final_df.head)
    final_df.to_csv("malaria/images/training_data.csv", index = False, header= False)

    print(final_df['class_name'].unique())

    class_dict = {
        'class': [], 
        'id': []
    }
    for idx, class_id in enumerate(final_df['class_name'].unique()):
        class_dict['class'].append(class_id)
        class_dict['id'].append(idx)

    class_df = pd.DataFrame(class_dict)
    print(class_df.head)

    class_df.to_csv("malaria/images/class_IDs.csv", header= False, index=False)


def crawl_through_imagej_files(master_folder):
    """Iterate through all of the CSV files containing the bounding box coordinates
    Generated by labeling in imageJ, and condenses them to a single csv file 
    compatible with CSVGenerator. 
    
    Arguments:
        master_folder sting -- the file location given the relative location of the 
            folder containing the csv files. 
    
    Returns:
        Pandas Dataframe -- A pandas DataFrame comtaining all the condensed image 
            coordinates in one table. 
    """
    dfs = []
    # Walk throughy all the files in the master folder. 
    for root, _, csv_files in os.walk(master_folder):
        # For all the csv files in the directory
        for csv_file in csv_files:
            # File location
            coordinate_location = "{}/{}".format(root, csv_file)
            # See if the CSV file is not empty
            try:
                # Read the csv file
                temp = pd.read_csv(coordinate_location)
                # The class variable
                temp['class_name'] = 'class 1'
            # If the Dataframe is empty emter enpty values
            except pd.errors.EmptyDataError:
                temp = {
                    'BX': [''],
                    'BY' : [''], 
                    'Width': [''], 
                    'Height' : [''],
                    'class_name' : ['']
                }
                temp = pd.DataFrame(temp)
                # Write the filename to file
            finally:
                problem_files = [
                    # "Core8A-UNMIX_crop", 
                    # "Core8B-UNMIX_crop", 
                    # "Unmixing_TMA_orientation_core_crop"
                    ]
                if sum([problem in csv_file[:-4] for problem in problem_files]) > 0:
                    # split_name = csv_file[:-4].split("_")
                    # print(split_name)
                    # temp['filename'] = "_".join(split_name[:-2]) + "_channel_0_" + split_name[-1][:3]+ "_" + split_name[-1][-1]+ ".tif"
                    temp['filename'] = csv_file[:-4].replace("crop", "crop_channel_0").replace("img", "img_") + '.tif'
                    print(temp['filename'])
                else:
                    temp['filename'] = csv_file[:-4] + ".tif"
                dfs.append(temp)
    # Combine all 
    dfs_combined = pd.concat(dfs)
    return dfs_combined

def make_retinanet_compatible(df):
    """Convert the dataframe of all the concatenated coordinates into one 
    large dataframe. The CSVGenerator has a specific coordinate setup 
    containing the variables filename,x1, y1, x2, y2, class. 
    
    Arguments:
        df {Pandas DataFrame} -- The conatenated dataframe of all the bounding box coordinates. 
    
    Returns:
        Pandas DataFrame -- The CSVGenerator compatible dataframe. 
    """
    df['x1'] = df['BX']
    df['y1'] = df['BY']
    df['x2'] = df['BX'] + df['Width']
    df['y2'] = df['BY'] + df['Height']
    final = df.loc[((df['x1'] != df['x2']) & (df['y1'] != df['y2'])) | ((df['x1'] == "") & (df['y1'] == ""))]
    return final[['filename', 'x1', 'y1', 'x2', 'y2', 'class_name']]


def archive_table(df):
    df.to_csv("images/training_data.csv", header = False, index = False)
    anchor_optimization = df.loc[df['x1'] != ""]
    anchor_optimization.to_csv('images/optimization_table.csv', header = False, index = False)

if __name__ == "__main__":
    df = crawl_through_imagej_files('images/bounding_box_coords')
    df_fixed = make_retinanet_compatible(df)
    archive_table(df_fixed)

    print(df_fixed.head())

    crawl_through_json_files('malaria/training.json')