import numpy as np
from PIL import Image
import glob
import math

dir = 'two_color_images/'
save_dir = 'processed_two_color_images/'

for image_filename in glob.iglob('{}*_DAPI.tif'.format(dir)):
    num_tiles = 40  # 40 tiles per image
    channel2_fn = '{}_CD3.tif'.format(image_filename[:-9])
    image_array_channel1 = np.array(Image.open(image_filename))
    image_array_channel2 = np.array(Image.open(channel2_fn))
    print(image_filename, image_array_channel1.shape)
    print(channel2_fn, image_array_channel2.shape)

    area = image_array_channel1.shape[0]*image_array_channel1.shape[1]
    tile_area = area//num_tiles
    tile_dim = int(math.sqrt(tile_area))  # assuming a square

    x_segments = image_array_channel1.shape[0] // tile_dim
    y_segments = image_array_channel1.shape[1] // tile_dim

    print(x_segments, y_segments)

    fn_prefix_ch1 = image_filename[len(dir):-4]
    fn_prefix_ch2 = channel2_fn[len(dir):-4]

    i = 0
    for x in range(x_segments):
        for y in range(y_segments):
            # Crop channel 1
            cropped_ch1 = image_array_channel1[x * tile_dim:(x + 1) * tile_dim, y * tile_dim:(y + 1) * tile_dim]
            cropped_im_ch1 = Image.fromarray(cropped_ch1)
            fn_ch1 = '{}{}_img{}.tif'.format(save_dir, fn_prefix_ch1, i)
            cropped_im_ch1.save(fn_ch1)

            # Crop channel 2
            cropped_ch2 = image_array_channel2[x * tile_dim:(x + 1) * tile_dim, y * tile_dim:(y + 1) * tile_dim]
            cropped_im_ch2 = Image.fromarray(cropped_ch2)
            fn_ch2 = '{}{}_img{}.tif'.format(save_dir, fn_prefix_ch2, i)
            cropped_im_ch2.save(fn_ch2)

            i += 1




