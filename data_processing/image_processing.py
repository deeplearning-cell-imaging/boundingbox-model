"""Script file for processing the image provided by Vermont Microscopy Imaging Center. 
"""
import os
import glob

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


def segment_all_images_in_directory(containing_directory, single_channel = True, channel_label = ""):
    """Segment all images in a directory into smaller subsections of one channel images
    
    Arguments:
        containing_directory {string} -- the filepath of the file containing the un-segmented images 
    
    Keyword Arguments:
        single_channel {bool} -- Is the image a chingle channel image, or multichanncel (color) image. (default: {True})
        channel_label {str} -- What to label the channel as in the final filenames (default: {""})
    
    Returns:
        dict -- a dictionay as returned by segment_tiffs()
    """
    # Segment all the images in a directory from larger tiff files to smaller ones. 
    all_images = {}
    # Iterate through all .tif files in the given directory. 
    for image_filename in glob.iglob('{}/*.tif'.format(containing_directory)):
        # Initialize the subdictionary 
        single_image = {}
        # If only a single channel 
        if single_channel:
            # Read in the image as a numpy array
            image_array = np.array(Image.open(image_filename))
            # Set as the label, otherwise, set as zero. 
            single_image['{}'.format(0 if channel_label == "" else channel_label)] = image_array
        else:
            # Set the subdictionary as a two key/value pair dictionary 
            single_image = import_tiff_file_multichannel(image_filename)
        # Set the filename as the key
        all_images[image_filename] = single_image
    # Segment all the images in the dictionary
    segmented = segment_tiffs(all_images, 1024)
    return segmented


def import_tiff_file_multichannel(filename):
    """Import a multichannel tiff image and convert to a numpy array for each channel 
    (color) and store the image in a dictionary, with one key for each channel mapping 
    to a numpy array for that channel. 
    
    Arguments:
        filename {string} -- string containing the relative filelocation and filename for the image. 
    
    Returns:
        dict -- a dictionary, with a key for each channel that maps to a numpy array for that channel. 
    """
    # Open
    test = Image.open(filename)
    # Initialize the dictionary
    images = {}
    # Tiffs are multichannel. Seek each channel and add to dictionary as an array
    for i in range(2):
        test.seek(i)
        images[i] = np.array(test)
    return images


def segment_tiffs(imgs, new_size):
    """Segment multi-channel tiff images into smaller size as defined in the 
    new size argument. 
    
    Arguments:
        imgs {dict} -- A dictionary of all the images in the directory. The Keys are 
            the filenames, which maps to a subdictionary. Within the subdictionary, 
            there's a key for each channel in the image. the key then maps to the 
            numpy array for that channel. 
        new_size {int} -- the size of the new array as a dimension for each side of the array. 
    
    Returns:
        dict -- a dictionary which each key as the filename that maps to a subdictionary. 
            Withing each subdictionary, there's a key for each channel that maps to a list of 
            all the segmented images from the original image containined in the mainkey/filename., 
    """
    segmented_imgs = {}
    # Enumerate through all the images in the dictionary img is the filename, which maps to 
    # another dictionary containing the filenames for the images. 
    for idx, img in enumerate(imgs):
        #img should be the filename maps to two channels
        segmented_imgs[img] = {}
        # For each channel in the image. 
        for channel in imgs[img]:
            # Initialize a list for the channel
            segmented_imgs[img][channel] = []
            # Get the dimensions for the image
            x_dim = imgs[img][channel].shape[0]
            y_dim = imgs[img][channel].shape[1]
            # For the max dimension, get the number of segments. 
            segments = max(x_dim, y_dim) // new_size
            # Iterate through tiles (segments) in the image. 
            for x in range(segments):
                for y in range(segments):
                    # Crop the image
                    cropped = imgs[img][channel][x*new_size:(x + 1) * new_size, y*new_size:(y + 1) * new_size]
                    # Append to the cropped image list. 
                    segmented_imgs[img][channel].append(cropped)
    return segmented_imgs



def save_segments(segmented_imgs):
    """Save the dictionary containing the segmented images to file. Iterates through
    the dictionary as returned by segment_all_images_in_directory. 
    
    Arguments:
        segmented_imgs {dict} -- a dictionary as returned by segment_all_images_in_directory() 
            where the each key is a filename that maps to another dictionary where the key/value 
            pair where the key is the channel and the value is the list of segmented images. 
    """
    # For all the filenames
    for image_filename in segmented_imgs:
        # For each channel for that filename
        for channel in segmented_imgs[image_filename]:
            # Iterate through all images in the list of segmented images. 
            for idx, img in enumerate(segmented_imgs[image_filename][channel]):
                # Read the image
                temp = Image.fromarray(img)
                # NOTE: If problems arise with the Mac OS, this might be the cause, there's a 
                # difference in the backslash, forward slash
                # Get the filepath
                wo_filepath = image_filename.replace('.tif', '').split('\\')
                # Save the image with the original filename, the channel, and the image count in 
                # the list
                temp.save('images/{}_channel_{}_img_{}.tif'.format(wo_filepath[-1], channel, idx))


if __name__ == "__main__":

    containing_directory = "unprocessed_images"
    all_imgs = segment_all_images_in_directory(containing_directory)

    save_segments(all_imgs)