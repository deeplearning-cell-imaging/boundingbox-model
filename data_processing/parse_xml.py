
""" 
parsing xml in one of the training images provided in the data set and 
convertingto a CSV Generator class that can be used by the keras-retinanet model.  


WIP: Originally used for alternate datasets but no longer needed
"""
import xml.etree.ElementTree as ET

def read_xml_annotations():
    xml_doc = ET.parse('alt_imgs/erythroblast_dapi_class/2018/Annotations/063751.xml')
    root = xml_doc.getroot()

    bb_data = []
    for child in root.iter('object'):
        bb = {}
        for item in child.iter('name'):
            bb['class'] = item.text
        for box in child.iter('bndbox'):
            for xmin in box.iter('xmin'):
                bb['x1'] = xmin.text
            for ymin in box.iter('ymin'):
                bb['y1'] = ymin.text
            for xmax in box.iter('xmax'):
                bb['x2'] = xmax.text
            for ymax in box.iter('ymax'):
                bb['y1'] = ymax.text
        bb_data.append(bb)

    return bb_data



if __name__ == "__main__":
    read_xml_annotations()
