"""Visualisations
"""
from PIL import Image
import pandas as pd
import numpy as np
import seaborn as sns

from keras_retinanet.utils.visualization import draw_box
from keras_retinanet.utils.image import read_image_bgr

import matplotlib.pyplot as plt
import cv2


def draw_boxes_on_image(img_data, random_choice = True, pic_index = 0):
    """For an image, draw boxes on the image with all the bounding boxes 
    in the dataset passed as an argument. Can draw boxes on a random image 
    or an image passed as an argument. 
    
    Arguments:
        img_data {pandas Dataframe} -- A Dataframe compatible with CSVGenerator. The 
            columns are the filename as a tring, x1, y1, x2, x2, and the class. The dataset
            has one row for each bounding box. An image can have multiple rows. 
    
    Keyword Arguments:
        random_choice {bool} -- Whether to choose a random picture from the dataset. (default: {True})
        pic_index {int} -- which image to pick, only valid if random choice is false. (default: {0})
    
    Returns:
        bgr image -- a bgr image with all the bounding boxes drawn on the image. 
    """
    # Get a list of all the unique filenames
    images = img_data['filename'].unique()
    # If choosing a random choice, then randomly pick and index from all the unique filenames
    if random_choice:
        pic_index = np.random.randint(len(images))
    
    # Get all the bounding boxes that belong to that image. 
    to_draw = img_data.loc[img_data['filename'] == images[pic_index]]

    # Read in as a BGR image. 
    img = read_image_bgr(images[pic_index])

    # Make a copy to draw boxes on 
    draw = img.copy()
    # Convert to color version. 
    draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)
    # Draw the box. 
    to_draw.apply(apply_box, args = (draw,), axis = 1)
    return draw 


def apply_box(row, img):
    """Draw the box on the passed image from the coordinates provided in the row.
    this is an apply function that get applied to each row in a dataframe using 
    the apply() function from pandas. Uses draw_box from the keras-retinanet package. 
    
    Arguments:
        row {pandas Dataframe} -- a row from the pandas Dataframe described in 
            draw_boxes_on_image
        img {bgr image} -- and image as made in draw_boxes_on_image
    """
    draw_box(img, (row['x1'], row['y1'], row['x2'], row['y2']), color = [255, 0, 0])


def plot_results(filename):
    """Plot the training and validation loss over epochs. 
    
    Arguments:
        filename {string} -- relative filepath and filename to read in the data from 
            before plotting. 
    """
    data = pd.read_csv(filename)
    data.columns = ['epoch', 'loss', 'data_type']
    plt.figure(figsize = (10, 7))
    sns.lineplot(x = 'epoch', y = 'loss',  style = 'data_type', data = data, ci = None, 
            palette = sns.color_palette('bright', 3))
    sns.scatterplot(x = 'epoch', y = 'loss',  style = 'data_type', data = data, ci = None, 
            palette = sns.color_palette('bright', 3))


if __name__ == "__main__":
    img_data = pd.read_csv('training/training_data.csv', names= ['filename', 'x1', 'y1', 'x2', 'y2', 'class'])
    draw_boxes_on_image(img_data)
