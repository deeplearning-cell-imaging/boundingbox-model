

def archive_training_history(history, filename):
    """Write the history object to file, writing the epochs, loss, the condition, 
    and whether this is results from the training or validation testing set. 
    
    Arguments:
        history_list {History Object} -- a history object returned by Model.fit from a keras
            model 
        filename {string} -- the filename with the parent directory included. No file extension. 
        condition {[type]} -- the condition testing, will be written as the third coloumn in the 
            dataframe. 
    """
    fout = open('results/{}.txt'.format(filename), 'w')
    # for history in history_list:
        # Print training loss
    for idx, loss in enumerate(history['loss']):
        print(idx, loss, 'training', sep = ',', file = fout)
    # Print the validation loss
    if 'val_loss' in history.keys():
        for idx, val_loss in enumerate(history['val_loss']):
            print(idx, val_loss, 'testing', sep = ',', file = fout)
    fout.close()