#!/bin/bash
# specify a partition
#SBATCH --partition=dggpu
# Request nodes
#SBATCH --nodes=1
# Request some processor cores
#SBATCH --ntasks=32
# Request GPUs
#SBATCH --gres=gpu:8
# Request memory 
#SBATCH --mem=16G
# Maximum runtime of 30 minutes
#SBATCH --time=20:00:00
# Name of this job
#SBATCH --job-name=retinanet_train
# Output of this job, stderr and stdout are joined by default
# %x=job-name %j=jobid
#SBATCH --output=output/retinanet_train%x_%j.out
#SBATCH --mail-user=cklopfer@uvm.edu
#SBATCH --mail-type=ALL
# change to the directory where you submitted this script
cd ${SLURM_SUBMIT_DIR}
# your job execution follows:
source activate dl_final
time python ~/cs395/final/boundingbox-model/train.py --weights=resnet50_coco_best_v2.1.0.h5 --config=config.ini csv ../boundingbox-model/training_data.csv ../boundingbox-model/images/class_IDs.csv --backbone resnet50 --epochs 25 --batch-size 32