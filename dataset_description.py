"""For plotting results from training, and anchor optimisation
"""
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
def main():
    training_data = pd.read_csv("images/training_data.csv", names = ['file', 'x1', 'y1', 'x2', 'y2', 'class'])

    file_ids = training_data.groupby('file',as_index= False).count()

    print("Printing Length: ", len(file_ids))

    print(file_ids.describe())

    anchor_description()
    
    plot_de_results()

    plot_differential_evolution()


def plot_de_results():
    
    result_files = [
        "initial_history_mobilenet160_1_backbone_testing_images_2020-05-03.txt", 
        "initial_history_resnet50_backbone_testing_images_2020-05-03.txt",
        "initial_history_resnet50_backbone_testing_2020-05-03.txt",
        "initial_history_vgg16_backbone_testing_images_2020-05-04.txt"
    ]

    backbones = [
        'mobilenet160',
        'resnet50', 
        'resnet50 (scratch)',
         'vgg16'
         ]

    df_list = []
    for idx, r_file in enumerate(result_files):
        temp = pd.read_csv(os.path.join("vacc_trained", "curves", r_file), names= ['epochs', 'loss', 'type'])
        temp['backbone'] = backbones[idx]
        df_list.append(temp)
    
    large_curve = pd.concat(df_list)

    print(large_curve.head)

    sns.lineplot(data = large_curve.loc[large_curve['epochs'] < 25], x = 'epochs', y = 'loss', hue = 'backbone', style = 'type')
    plt.show()


def plot_differential_evolution():
    
    fin = open('results/anchor_optimsation.txt', 'r')
    counts = []
    for line in fin.readlines():

        if "Number of labels that don't have any matching anchor" in line:
            count = int(line.split(":")[1])
            counts.append(count)

    
    sns.lineplot(x = range(len(counts)), y = counts)
    plt.show()


def anchor_description():
    training_data = pd.read_csv("images/training_data.csv", names = ['file', 'x1', 'y1', 'x2', 'y2', 'class'])

    training_data['width'] = training_data['x2'] - training_data['x1']
    training_data['height'] = training_data['y2'] - training_data['y1']

    # print(training_data.loc[(training_data['width'] <= 32) |  (training_data['height'] <= 32)][['height', 'width']].describe())

    print(training_data[['height', 'width']].describe())
    


if __name__ == "__main__":
    main()
