"""testing the backbones, this calls the train_backbones function. 
"""
import sys
import argparse
from train_backbones import train_model

allowed_backbones_full =  ['resnet50', 'resnet101', 'resnet152', 
    'mobilenet128', 'mobilenet160', 'mobilenet192', 'mobilenet224',
    'vgg16', 'vgg19', 'densenet121', 'densenet169',
    'densenet201']

allowed_backbones_short = [
    'vgg16', 
    # 'densenet121'
    # 'mobilenet128'
    # 'resnet152',
    # 'resnet101',
    'resnet50', 
    'mobilenet160_1', 
    ]

parser = argparse.ArgumentParser()
parser.add_argument("-e", default= 10, type = int, help = "Num Epochs")

parser.add_argument('-l', default = 0.01, type = float, \
help = 'The learning rate for the model, will work for all \
    types of optimizers.')

parser.add_argument('-split', default = "True", type = str, \
help = 'Whether to split the data')

parser.add_argument('-small', default= "False", type = str, \
    help = "Whenther to use a small subset of the data for speed \
        purposes.")

parser.add_argument('-train', default= "True", type = str, \
    help = "Whether to run training, used for eval debugging.")

parser.add_argument('-parent_dir', default= "images", type = str, \
    help = "Parent directory, options are 'images' for the VMC images and \
        'malaria'")


args = vars(parser.parse_args())

args['train'] = True if args['train'] == "True" else False
args['small'] = True if args['small'] == "True" else False
args['split'] = True if args['split'] == "True" else False
args['parent_dir'] = "malaria/images" if args['parent_dir'] == "malaria" else 'images'


backbone_testing_results = {}

def print_results(results):
    table_format = "{:30} | {:30}"
    print(table_format.format("Backbone", "mAP"))
    for backbone in results:
        print(table_format.format(backbone, results[backbone]))
        



for backbone in allowed_backbones_short:
    backbone_testing_results[backbone] = train_model(args['e'], args['split'], args['small'], args['train'], 
    backbone = backbone, experiment = "backbone_testing", parent_dir= args['parent_dir'])

    print_results(backbone_testing_results)
